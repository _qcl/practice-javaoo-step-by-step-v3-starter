package ooss;

import com.sun.org.apache.xpath.internal.operations.Equals;

public class Person {

    protected int id;
    protected String name;
    protected int age;
    public Person(int id, String name, int age) {
        this.age = age;
        this.name = name;
        this.id = id;
    }

    public String introduce() {
        return String.format("My name is %s. I am %d years old.",name,age);
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj)
            return true;
        if(obj == null || obj.getClass() != getClass())
            return false;
        return this.id == ((Person)obj).id;
    }
}
