package ooss;

import java.util.ArrayList;
import java.util.List;

public class Klass {

    private int number;
    private Student leader;
    private List<Teacher> teachers;
    private List<Student> students;

    public Klass(int number) {
        this.number = number;
        this.teachers = new ArrayList<>();
        this.students = new ArrayList<>();
    }

    public int getNumber(){
        return number;
    }

    @Override
    public int hashCode() {
        return number;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj)
            return true;
        if(obj == null || obj.getClass() != getClass())
            return false;
        return this.number == ((Klass)obj).number;
    }

    public void assignLeader(Student leader) {
        if(leader.isIn(this)){
            this.leader = leader;
            teachers.forEach(teacher -> teacher.say(this.number,this.leader.name));
            students.forEach(student -> student.say(this.number,this.leader.name));
        } else {
            System.out.println("It is not one of us.");
        }
    }

    public boolean isLeader(Student student) {
        return student.equals(this.leader);
    }

    public void attach(Teacher teacher) {
        if(!this.teachers.contains(teacher))
            this.teachers.add(teacher);
    }

    public void attach(Student student) {
        if(!this.students.contains(student))
            this.students.add(student);
    }
}
