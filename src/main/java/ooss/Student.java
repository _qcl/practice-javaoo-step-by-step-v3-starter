package ooss;

public class Student extends Person {

    private Klass klass;
    public Student(int age, String name, int id) {
        super(age, name, id);
    }
    public String introduce() {
        String myIntroduce = super.introduce() + String.format(" I am a student.");
        if(klass != null){
            if(klass.isLeader(this)){
                myIntroduce += String.format(" I am the leader of class %d.",klass.getNumber());
            } else {
                myIntroduce += String.format(" I am in class %d.",klass.getNumber());
            }
        }
        return myIntroduce;
    }

    public void say(int classNumber,String leader){
        System.out.printf("I am %s, student of Class %d. I know %s become Leader.%n",this.name,classNumber,leader);
    }
    public void join(Klass klass) {
        this.klass = klass;
    }

    public boolean isIn(Klass klass) {
        if(this.klass == null)
            return false;
        return this.klass.equals(klass);
    }
}
