package ooss;

import java.util.*;
import java.util.stream.Collectors;

public class Teacher extends Person {

    private Set<Klass> klassSet;
    public Teacher(int age, String name, int id) {
        super(age, name, id);
        klassSet = new HashSet<>();
    }
    public String introduce() {
        String myIntroduce = super.introduce() + " I am a teacher.";
        if(!klassSet.isEmpty()){
            myIntroduce +=  " I teach Class " + klassSet.stream().map(Klass::getNumber).map(String::valueOf).collect(Collectors.joining(", ")) + ".";
        }
        return myIntroduce;
    }

    public void say(int classNumber,String leader){
        System.out.printf("I am %s, teacher of Class %d. I know %s become Leader.%n",this.name,classNumber,leader);
    }

    public void assignTo(Klass klass) {
        klassSet.add(klass);
    }

    public boolean belongsTo(Klass klass) {
        return this.klassSet.contains(klass);
    }

    public boolean isTeaching(Student student) {
        return klassSet.stream().anyMatch(student::isIn);
    }
}
